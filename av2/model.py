from config import *

class Animal(db.Entity):
    cor = Required(str)
    raca = Required(str)
    nome = Optional(str)
    racoes = Set('Racao')

    def __str__(self):
        return f'''
    Nome {self.nome}
    Raca: {self.raca}
    Cor: {self.cor}
    

        '''
    
class Racao(db.Entity):
    marca = Required(str)
    sabor = Required(str)
    tamanho = Optional(str)
    animais = Set('Animal')

    def __str__(self):
        return f'''
    Marca {self.marca}
    Sabor: {self.sabor}
    Tamanho: {self.tamanho}
        '''

db.bind(provider='sqlite', filename='animal.db', create_db=True)
db.generate_mapping(create_tables=True)