from config import *
from model import *

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/listar_animais")
def listar_animais():
    with db_session:
        # obtém os animais
        animais = Animal.select() 
        return render_template("listar_animais.html", animais=animais)

@app.route("/form_adicionar_animais")
def form_adicionar_animal():
    return render_template("form_adicionar_animal.html")

@app.route("/adicionar_animal")
def adicionar_animal():
    # obter os parâmetros
    nome = request.args.get("nome")
    cor = request.args.get("cor")
    raca = request.args.get("raca")
    # salvar
    with db_session:
        # criar o animal
        p = Animal(**request.args)
        # salvar
        commit()
        # encaminhar de volta para a listagem
        return redirect("listar_animais") 

'''
run:
$ flask run
'''